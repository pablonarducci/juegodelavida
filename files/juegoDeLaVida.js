var configuracion = {
	velocidad: 1000,
	stayAlive: [2,3],
	resurrect: [3],
	pause: false,
	nextRand: () => configuracion.randomReal(),
	randomReal: Math.random,
	staticRandom: staticRandom.nextRand,
	changeClass: changeClass
}

var designs = {
	chess: function() {
		for (var i = 0; i < rows; i++) {
			for (var j = 0; j < cols; j++) {
				if (i%2 != j%2) {
					toggleClass.apply(fichas[i][j].elemento)
				}
			}
		}
	},
	vbars: function(n) {
		if (!n) {
			n = 10;
		}

		for (var i = 0; i < rows; i++) {
			for (var j = 0; j < cols; j++) {
				if (((j+1)%n) == 0) {
					toggleClass.apply(fichas[i][j].elemento)
				}
			}
		}
	},
	hbars: function(n) {
		if (!n) {
			n = 10;
		}

		for (var i = 0; i < rows; i++) {
			for (var j = 0; j < cols; j++) {
				if (((i+1)%n) == 0) {
					toggleClass.apply(fichas[i][j].elemento)
				}
			}
		}
	},
	random: function (r) {
		if (!r) {
			r = 0.2;
		}

		for (var i = 0; i < rows; i++) {
			for (var j = 0; j < cols; j++) {
				if (configuracion.nextRand() < r) {
					toggleClass.apply(fichas[i][j].elemento)
				}
			}
		}
	},
	clear: function () {
		for (var i = 0; i < rows; i++) {
			for (var j = 0; j < cols; j++) {
				configuracion.changeClass.apply(fichas[i][j].elemento, [false])
				fichas[i][j].vivo = 0
			}
		}
	}
}

$(function() {
    setOnClicks();
    generate();
	setFichas()
});

var cols = 100
var rows = cols /2
var fichas = []

function generate() {
	var content = "";
	var id = 0;
	for (var i = 0; i < rows; i++) {
		for (var j = 0; j < cols; j++) {
			content += "<div class= 'grid-black' id='" + id++ + "'>" + "</div>";
		}
	}
	$("#grids").html(content);

	$(document).keydown(function(e){
		if (e.keyCode==82 ) {
			run();
		} else if(e.keyCode==80) {
			configuracion.pause = !configuracion.pause
		} else if (e.keyCode==49 && e.ctrlKey) {
			designs.clear();
			designs.chess();
		} else if (e.keyCode==50 && e.ctrlKey) {
			designs.clear();
			designs.vbars();
		} else if (e.keyCode==51 && e.ctrlKey) {
			designs.clear();
			designs.hbars();
		} else if (e.keyCode==52 && e.ctrlKey) {
			designs.clear();
			designs.random();
		} else if (e.keyCode==48 && e.ctrlKey) {
			designs.clear();
		}
	});
}

function setFichas() {
	var id = 0;
	for (var i = 0; i < rows; i++) {
		var fila = []
		for (var j = 0; j < cols; j++) {
			fila.push({elemento: $( "#" + id ), id: id++, vivo: 0, vecinos: 0})
		}
		fichas.push(fila);
	}
}


function run() {
	if (configuracion.pause) {
		setTimeout(run, configuracion.velocidad);
		return;
	}

	var nuevasFichas = [];

	for (var i = 0; i < rows; i++) {
		var fila = []
		for (var j = 0; j < cols; j++) {
			var vecinos = contarVecinos(i, j);
			
			var este = fichas[i][j];
			var vivo = 0;

			if (este.vivo && configuracion.stayAlive.includes(vecinos)) {
				vivo = 1;
			}

			if (!este.vivo && configuracion.resurrect.includes(vecinos)) {
				vivo = 1;
			}			
			
			fila.push({vivo, vecinos})
		}
		nuevasFichas.push(fila);
	}

	for (var i = 0; i < rows; i++) {
		for (var j = 0; j < cols; j++) {
			fichas[i][j].vecinos = nuevasFichas[i][j].vecinos;
			fichas[i][j].vivo = nuevasFichas[i][j].vivo;
			configuracion.changeClass.apply(fichas[i][j].elemento, [nuevasFichas[i][j].vivo])
		}
	}

	setTimeout(run, configuracion.velocidad);
}

function contarVecinos(i, j) {
	return estaVivo(i-1, j-1) + estaVivo(i-1, j) + estaVivo(i-1, j+1) + estaVivo(i, j-1) + estaVivo(i, j+1) + estaVivo(i+1, j-1) + estaVivo(i+1, j) + estaVivo(i+1, j+1);
}

function estaVivo(i, j) {
	return i >= 0 && j >= 0 && i < rows && j < cols && fichas[i][j].vivo;
}


//Event delegation
function setOnClicks() {
    $("#grids").on("click", ".grid-black", toggleClass);
    
    $("#grids").on("click", ".grid-white", toggleClass);
}

function toggleClass() {		
	var id = $(this).attr('id')
	var index = parseInt(id);
	var row = Math.floor(index / cols)
	var col = index - (row * cols)
	var ficha = fichas[row][col]
	
	var value = $(this).attr('class');
	
	if (value == 'grid-black') {
		$(this).removeClass('grid-black');
		$(this).removeClass('grid-green');
		$(this).removeClass('grid-red');
		$(this).addClass('grid-white');
		ficha.vivo = 1;
	} else {
		$(this).removeClass('grid-white');
		$(this).removeClass('grid-green');
		$(this).removeClass('grid-red');
		$(this).addClass('grid-black');
	    ficha.vivo = 0
	}
}

function changeClass(estaVivo) {
	if(!estaVivo) {
		$(this).removeClass('grid-white');
		$(this).removeClass('grid-green');
		$(this).removeClass('grid-yellow');
		$(this).removeClass('grid-red');
		$(this).addClass('grid-black');
		return;
	}

	var value = $(this).attr('class');

	if(value == 'grid-black') {
		$(this).removeClass('grid-black');
		$(this).removeClass('grid-green');
		$(this).removeClass('grid-yellow');
		$(this).removeClass('grid-red');
		$(this).addClass('grid-white');
		return;
	}
	
	if(value == 'grid-white') {
		$(this).removeClass('grid-black');
		$(this).removeClass('grid-white');
		$(this).removeClass('grid-yellow');
		$(this).removeClass('grid-red');
		$(this).addClass('grid-green');
		return;
	}
	
	if(value == 'grid-green') {
		$(this).removeClass('grid-black');
		$(this).removeClass('grid-white');
		$(this).removeClass('grid-green');
		$(this).removeClass('grid-red');
		$(this).addClass('grid-yellow');
		return;
	}
	
	if(value == 'grid-yellow') {
		$(this).removeClass('grid-black');
		$(this).removeClass('grid-white');
		$(this).removeClass('grid-green');
		$(this).removeClass('grid-yellow');
		$(this).addClass('grid-red');
		return;
	}
}

function dullChangeClass(estaVivo) {
	if(!estaVivo) {
		$(this).removeClass('grid-white');
		$(this).removeClass('grid-green');
		$(this).removeClass('grid-yellow');
		$(this).removeClass('grid-red');
		$(this).addClass('grid-black');
		return;
	}
	
	$(this).removeClass('grid-black');
	$(this).removeClass('grid-green');
	$(this).removeClass('grid-yellow');
	$(this).removeClass('grid-red');
	$(this).addClass('grid-white');
}